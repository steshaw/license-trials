#!/usr/bin/env bash

for file in LICENSE*; do
  echo
  bundle exec licensee "$file"
done
